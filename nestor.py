#!/usr/bin/python

from contextlib import contextmanager
from datetime import date, timedelta
from enum import Enum
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException
from selenium.webdriver.chrome.options import Options

import json
import os
import re
import sys
import time

class Driver(Enum):
    CHROME = 0
    FIREFOX = 1

@contextmanager
def driver_context(drv):
    if drv == Driver.CHROME:
        options = Options()
        options.headless = True
        driver = webdriver.Chrome(options=options)
    elif drv == Driver.FIREFOX:
        driver = webdriver.Firefox()
    else:
        raise ValueError('Unsupported driver')
    try:
        yield driver
    finally:
        driver.quit()

def select_dropdown_element(fmtstr, value):
    i = 1
    while True:
        try:
            elem = driver.find_element_by_xpath(fmtstr.format(i))
            if elem.get_attribute('pd-infoobject-context-name').lower() == value.lower():
                elem.click()
                break
            i = i + 1
        except NoSuchElementException:
            raise ValueError('Dropdown value {} did not match any'.format(value))


with open('{}/.config/nestor.json'.format(os.environ.get('HOME')), 'r') as f:
    config = json.load(f)

timeout = config.get('timeout', 30)

times = config['times']
assert len(times) == 7

target_date = date.today() + timedelta(config.get('days_in_advance', 3))

day_idx = (date.today().weekday() + config.get('days_in_advance', 3)) % 7

time_to_book = times[day_idx]
if time_to_book == '':
    # No time set in config
    sys.exit()

openhr  = 4
openmin = 20

bookhr  = int(time_to_book.split(':')[0])
bookmin = int(time_to_book.split(':')[1])

drv = Driver.CHROME if config['driver'].lower() == "chrome" else Driver.FIREFOX

if bookhr < 4 or bookhr > 23:
    raise ValueError('Cannot book a time outside of opening hours')

if bookmin % 20:
    raise ValueError('Minutes must be a multiple of 20')

slot_idx = (bookhr - openhr) * 3 + (bookmin - openmin) / 20
idstr = '//*[@id="GroupTrainingPassDayList-{}"]/div[{}]/div[8]/div'.format(target_date, int(1 + slot_idx * 2))

with driver_context(drv) as driver:
    driver.implicitly_wait(10)
    driver.get(config['url'])

    WebDriverWait(driver, timeout).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="PASTELLDATA_WRAPPER_IFRAME_0"]')))
    driver.find_element_by_xpath('//*[@id="UserLoginInfoButton"]').click()
    driver.find_elements_by_xpath('//*[@id="UserName"]')[1].send_keys(config['username'])
    driver.find_elements_by_xpath('//*[@id="Password"]')[1].send_keys(config['password'])
    driver.find_element_by_xpath('//*[@id="QuickLoginForm"]/div[6]/div[1]/div/div').click()
    WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//*[@id="UserLoginInfoButton"]/div/div/div/div[2]/div')))
    WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//*[@id="viewtabcontrol"]/div/div[2]/div/div/div[1]/div/div/div[1]/div/div/div[{}]/div'.format(config.get('days_in_advance', 3) + 1)))).click()
    WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//*[@id="GTUnitFilterForm"]/div/div[1]'))).click()

    select_dropdown_element('//*[@id="UnitFilterComboFilter"]/div[{}]/div', config['facility'])

    driver.find_element_by_xpath('//*[@id="UnitFilterComboDropDownPanel"]/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/div/img').click()
    time.sleep(5)

    driver.find_element_by_xpath('//*[@id="GTActivityFilterForm"]/div/div[1]').click()

    select_dropdown_element('//*[@id="ActivityFilterComboFilter"]/div[{}]/div[1]', 'gymträning')

    driver.find_element_by_xpath('//*[@id="ActivityFilterComboDropDownPanel"]/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div/div[2]/div/img').click()
    time.sleep(5)

    book_button = driver.find_element_by_xpath(idstr)
    book_button.click()
    dialog_id = book_button.get_attribute('pd-post-modal-toggle-target')
    match = re.search('[Rr]eserv', book_button.text)
    book_xpath = '//*[@id="{}BookButton_{}"]'.format('Reserve' if match else '', dialog_id.split('-')[1])
    time.sleep(5)
    driver.find_element_by_xpath(book_xpath).click()
    while True:
        try:
            driver.find_element_by_xpath('//*[@id="bs-example-navbar-collapse-1"]/div/div[1]/div[1]/div/div/div[3]/div').click()
            break
        except ElementClickInterceptedException:
            time.sleep(1)

    fields = driver.find_element_by_xpath('//*[@id="GroupTraningBookingsList"]').text
    split = re.split(r'(\d{4}-\d{2}-\d{2})', fields)
    split = [x.replace('\n', ' ').lower() for x in split]

    for i in range(1, len(split), 2):
        date = split[i]
        content = split[i+1]
        if str(target_date) == date and                                 \
           re.search(time_to_book.replace(':', '\\.'), content) and     \
           re.search(config['facility'].lower(), content):
            sys.exit()

    sys.exit(1)
